package com.callback.example.callback.controller;

import com.callback.example.callback.service.CallBackService;
import com.callback.example.callback.models.CallBackError;
import com.callback.example.callback.models.CallBackSuccess;
import net.minidev.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class CallbackController {

    @Autowired CallBackService callBackService;

    @Autowired CallBackSuccess callBackSuccess;

    @Autowired CallBackError callBackError;

    @RequestMapping(method = RequestMethod.POST, value = "/notifications")
    public ResponseEntity<?> notifications(@RequestBody JSONObject requestBody) {
        if (requestBody.get("success").toString().equalsIgnoreCase("true")) {
            BeanUtils.copyProperties(requestBody, callBackSuccess);
            return callBackService.addNotification(true);
        } else {
            BeanUtils.copyProperties(requestBody, callBackError);
            return callBackService.addNotification(false);
        }

    }

}


