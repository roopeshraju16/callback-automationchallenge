package com.callback.example.callback.models;

import org.springframework.stereotype.Component;

@Component
public class CallBackResponse {
    int statusCode;
    String paymentStatus;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
}
