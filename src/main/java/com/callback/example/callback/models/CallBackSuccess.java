package com.callback.example.callback.models;

import org.springframework.stereotype.Component;

@Component
public class CallBackSuccess {
    private boolean success;
    private long transactionId;
    private Bill bill;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }
}
