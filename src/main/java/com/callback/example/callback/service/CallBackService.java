package com.callback.example.callback.service;

import com.callback.example.callback.models.CallBackResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CallBackService {

    @Autowired CallBackResponse callbackResponse;

    public ResponseEntity<?> addNotification(boolean success) {
                if (success)
                {
                    callbackResponse.setPaymentStatus("Success");
                    callbackResponse.setStatusCode(200);
                }

                else
                {
                    callbackResponse.setPaymentStatus("Failed");
                    callbackResponse.setStatusCode(200);
                }
    return new ResponseEntity<CallBackResponse>(callbackResponse,HttpStatus.OK);
    }
}
